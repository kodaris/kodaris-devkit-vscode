# Kodaris DevKit

Develop and test Kodaris websites with ease using our very own dev kit.

## Features

* **Add remote repository** - you can easily add your website's repository to edit it.
* **Push changes to repository** - directly push your changes when you're done editing.

## Requirements

* Visual Studio Code v1.63+ - you can download the latest version [here](https://code.visualstudio.com/).
* `webDeveloper` - make sure that your **Kodaris Account** has this permission.

## Installation

1. Go to the Extensions tab - `Ctrl+Shift+X`.
2. Search for **Kodaris Development Kit** and install it. This will install the latest version.
3. Reload Visual Studio Code. (Close it and open it again.)

## How-To

1. Clone your repository using git with - `git clone https://MY_AWESOME_WEBSITE.git` or Open **Visual Studio Code** and press `Ctrl+Shift+G` to go to the **Source Control** tab and click **Clone Repository** button.
    * You can also click **Open Folder** if you cloned the repository using the command line.

![source control tab!](https://code.visualstudio.com/assets/docs/editor/versioncontrol/firstrun-source-control.png)

2. Press `F1` and key in `Kodaris: Add Remote`. This will prompt for the following:
    * **Website URL**: url of the website that you want to edit.
    * **Username**: your Kodaris account username
    * **Password**: your Kodaris account password
3. Create your branch by either going to the **three-dot menu** under the **Source Control** tab and go to:
    * **Branch > Create Branch** - this will prompt you to create a new branch based on your current checked out branch. 
    * Give it a branch name - **KOD-4206**

![three-dot menu!](https://code.visualstudio.com/assets/docs/editor/versioncontrol/scm-more-actions.png)

4. Or go to to lower-left corner of the editor and click the git status indicator.
    * Click **Create a new branch...** and give it a name.

![status indicator!](https://code.visualstudio.com/assets/docs/editor/versioncontrol/git-status-bar-sync.png)

5. Next, you need to push your branch by pressing `F1` and key in - `Kodaris: Push`.

6. Lastly, you need to set the **Commerce Layout** by going to your website's API swagger (eg. https://commerce.kodaris.com) - under **User API** > **Customer API** > `/api/user/customer/setCommerceLayout` and key in your branch name as the `layout` parameter.
    * This parameter is case sensitive.

## Making Changes

Making changes is extremely easy with Kodaris DevKit. Just edit a file and save it, refresh the website you're currently editing and the changes will automatically be applied!

This codeblock represents the following part of the page:
```html
<div class="k-welcome__hero-actions">
    <a class="k-welcome__hero-button" href="#products">Product Information</a>
    <a class="k-welcome__hero-button" href="#applications">Industries Served</a>
</div>
```

![sample_1!](https://iili.io/E2sbaV.png)

Now, let's change it to:
```html
<div class="k-welcome__hero-actions">
    <a class="k-welcome__hero-button" href="#products">Awesome Products  Page!</a>
    <a class="k-welcome__hero-button" href="#applications">Our Amazing Apps!</a>
</div>
```

Save the file, refresh the website and it will automatically be reflected on the page:
![sample_2!](https://iili.io/E2spyP.png)

## To Do

* Set Commerce Layout via a command.
