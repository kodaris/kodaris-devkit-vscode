import { ExtensionContext } from 'vscode';
import * as vscode from 'vscode';
import axios, { Axios } from 'axios';
const FormData = require('form-data');
import { EventEmitter } from 'events';
import { EventSourceType, SSELogsConnectionEventType, SSELogsService } from './sse-logs.service';

const IGNORED_FOLDERS = ['.git', '.idea', '.DS_STORE'];

const FONT_FORMATS = ['eot', 'otf', 'ttf', 'woff', 'woff2'];
const IMAGE_FORMATS = ['apng', 'avif', 'bmp', 'gif', 'jpeg', 'jpg', 'ico', 'png', 'tiff', 'webp'];

enum DevKitEventType {
  Login = 'login',
  AddCredentials = 'addCredentials',
  RemoveCredentials = 'removeCredentials'
}

export class KodarisDevKit {
    private _devKitEvents = new EventEmitter();

    private _git: any;

    private _repo: any;

    private _workspaceFolder!: vscode.WorkspaceFolder;

    // private _currentBranch: string|undefined;

    private _url: string|undefined;

    private _axios!: Axios;

    private _logChannel: vscode.OutputChannel;

    private _sseLogsConnection: EventSourceType;

    private _sseLogsEvents: EventEmitter|undefined;

    private _sseLogsService = new SSELogsService();

    private _sseCloseTimeout: ReturnType<typeof setTimeout> | null = null;

    private _isProductionWebDeveloper: boolean = false;

    private _hostFolder: string = '';

    private _tmpHostTesting: boolean = false;

    constructor(private _context: ExtensionContext) {
        this._url = this._context.workspaceState.get('remote_url');
        this._logChannel = vscode.window.createOutputChannel('Kodaris Devkit Extension Logs');
        this._initAxios();
        this._initGit();
        this._initSSELogs();
        this._initUser();
    }

    isValid() {
        console.log('checking if ext is valid', this._url, this._getCurrentBranch());
        this._logChannel.append(`checking if ext is valid; url = ${this._url}, branch = ${this._getCurrentBranch()}`);

        if (!this._url) {
            console.log('there is no current url, so invalid', this._url);
            this._logChannel.append(`there is no current url, so invalid; url = ${this._url}`);
            return false;
        }

        if (!this._getCurrentBranch()) {
            console.log('there is no current branch, so invalid', this._getCurrentBranch());
            this._logChannel.append(`there is no current branch, so invalid; branch = ${this._getCurrentBranch()}`);
            return false;
        }

        if (this._isProductionWebDeveloper) {
            console.log('current user is a production web developer so valid', this._isProductionWebDeveloper);
            this._logChannel.append(`current user is a production web developer so valid; isProductionWebDeveloper = ${this._isProductionWebDeveloper}`);
            return true;
        }

        if (this._getCurrentBranch() === 'staging') {
            console.log('current branch is staging, so invalid', this._getCurrentBranch());
            this._logChannel.append(`current branch is staging, so invalid; branch = ${this._getCurrentBranch()}`);
            return false;
        }

        if (this._getCurrentBranch() === 'production') {
            console.log('current branch is production, so invalid', this._getCurrentBranch());
            this._logChannel.append(`current branch is production, so invalid; branch = ${this._getCurrentBranch()}`);
            return false;
        }

        if (this._getCurrentBranch() === 'master') {
            console.log('current branch is master, so invalid', this._getCurrentBranch());
            this._logChannel.append(`current branch is master, so invalid; branch = ${this._getCurrentBranch()}`);
            return false;
        }

        console.log('all checks passed, ext is valid');
        this._logChannel.append(`all checks passed, ext is valid`);
        return true;
    }

    login() {
        return new Promise((resolve, reject) => {
            this._context.secrets.get(`${this._url}_remote_username`)
                .then(username => {
                    this._context.secrets.get(`${this._url}_remote_password`).then(password => {
                        // clear old cookie otherwise the sessions get mixed up
                        (this._axios.defaults.headers as any).Cookie = null;
                        this._axios.get(`${this._url}/api/user/employee/authToken`)
                            .then((token: any) => {
                                return this._axios.post(`${this._url}/api/user/employee/login`, null, {
                                    params: {
                                        userName: username,
                                        password: password
                                    },
                                    headers: {
                                        'X-CSRF-TOKEN': token
                                    }
                                });
                            })
                            .then((res: any) => {
                                console.log(`Successfully logged in`, res);
                                resolve(res);
                            })
                            .catch((err: any) => {
                                console.log(`Exception logging in`, err);
                                reject(err);
                            });
                    });
                });
        });
	}

    fetchRoles() {
        return this._axios.get(`${this._url}/api/system/employee/roles`) as any;
	}

    fetchHost() {
        return this._axios.get(`${this._url}/api/system/host/currentHost`) as any;
	}

    saveFolder(folder: vscode.Uri) {
        console.log(`Attempting to save folder in kodaris vfs`, folder);

        const s3Path = this._getS3Path(folder.path);
        const subpath = s3Path.split(/\/|\\/g).slice(0,-1).join('/');
        const foldername = s3Path.split(/\/|\\/g).pop();

        return this._axios.post(`${this._url}/api/system/website/folder`, null, {
            params: {
                subFolderPath: subpath,
                newFolderName: foldername
            }
        }).catch(err => {
            console.error(`Exception creating folder in kodaris vfs`, folder.path, err);
            vscode.window.showErrorMessage(`Failed to save folder in kodaris vfs: ${folder.path}`);
            return Promise.reject(err);
        });
	}

    saveFile(file: vscode.Uri) {
        console.log(`Attempting to save file in kodaris vfs`, file);

        const s3Path = this._getS3Path(file.path);
        const subpath = s3Path.split(/\/|\\/g).slice(0,-1).join('/');
        const filename = s3Path.split(/\/|\\/g).pop();
        const format = filename ? filename.substring(filename.indexOf('.') + 1, filename.length).toLowerCase() : '';

        console.log(`The file s3 path is`, s3Path);

        return new Promise((resolve, reject) => {
            vscode.workspace.fs.readFile(file).then(rawContent => {
                const formData = new FormData();
                let fileContent = rawContent;

                const isFont = FONT_FORMATS.includes(format);
                const isImages = IMAGE_FORMATS.includes(format);
                if (!isFont && !isImages) {
                    fileContent = Buffer.from(rawContent.toString() || '');
                }

                formData.append('file', fileContent, filename);

                return this._axios.post(`${this._url}/api/system/website/file`, formData, {
                    retryMethod: () => this.saveFile(file),
                    headers: formData.getHeaders(),
                    params: {
                        subFolderPathInCategoryFolder: subpath
                    }
                } as any)
                .then(res => {
                    resolve(res);
                    // return Promise.resolve();
                })
                .catch(err => {
                    console.error(`Exception creating file to kodaris vfs`, file.path, err);
                    vscode.window.showErrorMessage(`Failed to save file to kodaris vfs: ${file.path}`);
                    reject(err);
                    // return Promise.reject();
                });
            });
        });
	}

    saveFiles(files: vscode.Uri[]) {
        console.log(`Attempting to save files to kodaris vfs`, files);
        for (const file of files) {
            if (file.path.indexOf('.') === -1) {
                this.saveFolder(file);
            } else {
                this.saveFile(file);
            }
        }
    }

    renameFolder(oldUri: vscode.Uri, newUri: vscode.Uri) {
        return new Promise((resolve, reject) => {
            console.error(`Exception renaming folder in kodaris vfs. Unsupported`, oldUri.path, newUri.path);
            vscode.window.showErrorMessage(`Renaming folders is unsupported at this time: ${oldUri.path}`);
            reject();
        })
	}

    renameFile(oldUri: vscode.Uri, newUri: vscode.Uri) {
        return this.saveFile(newUri)
            .then(res => this.deleteFile(oldUri))
            .catch(err => {
                console.error(`Exception renaming file in kodaris vfs`, oldUri.path, newUri.path, err);
                vscode.window.showErrorMessage(`Error renaming file in kodaris vfs: ${oldUri.path}`);
                return Promise.reject(err);
            });
	}

    renameFiles(files: {oldUri: vscode.Uri, newUri: vscode.Uri}[]) {
        for (const file of files) {
            if (file.oldUri.path.indexOf('.') === -1) {
                this.renameFolder(file.oldUri, file.newUri);
            } else {
                this.renameFile(file.oldUri, file.newUri);
            }
        }
    }

    deleteFolder(folder: vscode.Uri) {
        const s3Path = this._getS3Path(folder.path);
        const subpath = s3Path.split(/\/|\\/g).slice(0,-1).join('/');
        const foldername = s3Path.split(/\/|\\/g).pop();
        return this._axios.delete(`${this._url}/api/system/website/folder`, {
            retryMethod: () => this.deleteFolder(folder),
            params: {
                subFolderPath: subpath,
                folderName: foldername
            }
        } as any);
    }
    
    deleteFile(file: vscode.Uri) {
        const s3Path = this._getS3Path(file.path);
        return this._axios.delete(`${this._url}/api/system/website/file`, {
            params: {
                filePath: s3Path
            }
        });
    }

    deleteFiles(files: vscode.Uri[]) {
        for (const file of files) {
			if (file.path.indexOf('.') === -1) {
				this.deleteFolder(file);
			} else {
				this.deleteFile(file);
			}
		}
    }

    async addRemote() {
        console.log('Attempting to connect workspace to a kodaris system');

        // fixme - throw error if no git repository
		    // const gitRootUri = this._git.repositories[0].rootUri.path;

		    const url = await vscode.window.showInputBox({ title: 'Url', prompt: 'Enter your website url', placeHolder: 'https://commerce.kodaris.com', ignoreFocusOut: true });

		    const username = await vscode.window.showInputBox({ title: 'Username', prompt: 'Enter your username', placeHolder: 'john.doe@kodaris.com', ignoreFocusOut: true });
		    /// await authProvider.storeUsername(username);

		    const password = await vscode.window.showInputBox({ title: 'Password', password: true, prompt: 'Enter your password', ignoreFocusOut: true });
		    // await authProvider.storePassword(password);

		    console.log('Collected the following connection details', url, username);

        if (url && username && password) {
            const stripUrl = url.endsWith('/') ? url.slice(0, -1) : url;
            this._url = stripUrl;
            await this._context.workspaceState.update('remote_url', stripUrl);
            await this._context.secrets.store(`${this._url}_remote_username`, username);
            await this._context.secrets.store(`${this._url}_remote_password`, password);

            this._devKitEvents.emit(DevKitEventType.AddCredentials);
            console.log('Successfully saved all login details: url, username & password');

            this._initUser();
        }
    }

    removeRemote() {
        console.log('Attempting to disconnect workspace from a kodaris system');
        this._context.secrets.delete(`${this._url}_remote_username`);
        this._context.secrets.delete(`${this._url}_remote_password`);
        this._context.workspaceState.update('remote_url', '');
        this._url = undefined;
        this._devKitEvents.emit(DevKitEventType.RemoveCredentials);
    }

    push() {
        console.log('Attempting to push branch to kodaris vfs');

        // TODO - enforce this serverside
        if (!this._isProductionWebDeveloper && (this._getCurrentBranch() === 'staging' || this._getCurrentBranch() === 'production' || this._getCurrentBranch() === 'master')) {
          vscode.window.showErrorMessage(`You do not have write access to branch ${this._getCurrentBranch()}`);
          console.error(`Error creating branch in kodaris vfs. User does not have access to ${this._getCurrentBranch()}`);
          return new Promise((resolve, reject) => reject());
        }

        this.deleteFolder(this._workspaceFolder.uri)
            .then(res => this._uploadFolder(this._workspaceFolder.uri))
            .then(res => {
                vscode.window.showInformationMessage(`Successfully pushed to branch ${this._getCurrentBranch()}`);
            })
            .catch(err => {
                vscode.window.showErrorMessage(`Error pushing to branch ${this._getCurrentBranch()}`, err);
                return Promise.reject(err);
            });
    }

    startSSELogging() {
        this._openSSELogs();
    }

    private async _uploadFolder(folder: vscode.Uri) {
        // fixme handle gitignore file
        console.log('uploading folder', folder.path);
    
        const directory = await vscode.workspace.fs.readDirectory(folder);
    
        console.log('read folder contents', folder.path, directory);
            
        for (const node of directory) {
            const nodePath = folder.path + "/" + node[0];
            if (IGNORED_FOLDERS.includes(node[0])) {
                continue;
            }
            if (node[1] === 2) {
                await this.saveFolder(vscode.Uri.parse(nodePath));
                await this._uploadFolder(vscode.Uri.parse(nodePath));
            } else {
                await this.saveFile(vscode.Uri.parse(nodePath));
            }
        }
    }

    private _getCurrentBranch() {
        return this._repo?.state.HEAD?.name;
    }

    private _getRelativePath = (absolutePath: string) => {
        this._logChannel.appendLine(`****** Getting relative path from absolute path ******: ${absolutePath}`);
        this._logChannel.appendLine(`Absolute path is: ${absolutePath}`);
        this._logChannel.appendLine(`Workspace folders are: ${JSON.stringify(vscode.workspace.workspaceFolders)}`);

        const workspacePath = vscode.workspace.workspaceFolders
            ?.map(folder => folder.uri.path)
            .filter(path => path !== '/' && path !== '\\' && absolutePath.toLowerCase()?.startsWith(path.toLowerCase()))[0];

        this._logChannel.appendLine(`Workspace path is: ${workspacePath}`);
            
        const relativePath = absolutePath.replace(new RegExp(`^${workspacePath}`), "");

        this._logChannel.appendLine(`Relative path is: ${relativePath}`);

        return relativePath;
    }
    
    private _getS3Path = (absolutePath: string) => {
        this._logChannel.appendLine(`****** Getting s3 path from absolute path ******`);
        this._logChannel.appendLine(`Absolute path is: ${absolutePath}`);

        const relativePath = this._getRelativePath(absolutePath);

        let s3Path = '';
        if (this._hostFolder) {
            this._logChannel.appendLine(`Host folder is: ${this._hostFolder}`);
            s3Path = `/${this._hostFolder}/${this._getCurrentBranch()}/website`;
        } else if (this._tmpHostTesting) {
            s3Path = `/default/${this._getCurrentBranch()}/website`;
        } else {
            s3Path = `/${this._getCurrentBranch()}`;
        }

        s3Path = s3Path + relativePath.replace(/\\/g, '/');

        this._logChannel.appendLine(`S3 path is: ${s3Path}`);

        return s3Path;
    }

    private _initUser() {
        if (!this._url) return;
        this.login()
            .then(() => {
                return this.fetchRoles();
            })
            .then((roles: any[]) => {
                this._isProductionWebDeveloper = !!roles?.filter(r => r.code === 'productionWebDeveloper')?.length;
            })
            .then(() => {
                return this.fetchHost();
            })
            .then((host: any) => {
                if (host && host.parentCode) {
                    this._hostFolder = host.parentCode;
                } else if (host && host.code) {
                    this._hostFolder = host.code;
                }
                this._tmpHostTesting = !!this._hostFolder || (!!this._url && (this._url.indexOf("wowser.kodaris") > -1 || this._url.indexOf("southeast") > -1));
            })
            .catch(err => {
                console.error("An error occurred initing user", err);
            });
    }

    private _initAxios() {
        // Preserve the kodaris user session between api requests.
        // Extract the response data so we don't have to do so everywhere in the extension.
        // Login and re-execute request if the user's session has expired.
        this._axios = axios.create({});
        this._axios.interceptors.response.use(res => {
            if (res.headers["set-cookie"]) {
                (this._axios.defaults.headers as any).Cookie = res.headers["set-cookie"][0];
            }
            if (res.data && res.data.success) {
                return res.data.data;
            }
            return res.data;
            }, err => {
                if (err.config.url.indexOf("login") == -1
                    && err.config.url.indexOf("authToken") == -1
                    && err.response && err.response.status === 401
                ) {
                    return this.login()
                        .then(res => {
                            if (err.config.retryMethod) {
                                // Any request that contains form data needs implement their own
                                // retry method as once the server reads the stream once it won't
                                // do it again and will result in a socket timeout exception.
                                return err.config.retryMethod();
                            }
                            return this._axios.request({...err.config});
                        });
                }
                return Promise.reject(err);
            });
    }

    // We use git as a reference when creating branches in the kodaris vfs
    // onDidChangeState is invoked when git has initialized and is ready for use.
    // We store the current repo, coresponding workspace folder, and attempt to get
    // the current branch. Sometimes it isn't initialize yet though and everytime
    // git runs an operation we try to get it. We also listen for when the user changes
    // branches and update our variable accordingly.
    private _initGit() {
        console.log('init git');        
        
        this._git = vscode.extensions.getExtension<any>('vscode.git')?.exports?.getAPI(1); //await this._getBuiltInGitApi(); //vscode.extensions.getExtension<any>('vscode.git')?.exports?.getAPI(1);
        console.log('finished initializing git');

        if (!this._git) {
            console.log('git api does not exist');
            return;
        }

        console.log('git api exists, extracting repo and branch');

        const init = () => {
            this._repo = this._git.repositories[0];
            // this._currentBranch = this._repo?._repository?._HEAD?.name;
            this._workspaceFolder = vscode.workspace.getWorkspaceFolder(this._repo.rootUri)!;

            // console.log('The git repo, branch, and folder are', this._repo, this._getCurrentBranch(), this._workspaceFolder);

            this._repo.repository.onDidRunOperation((ev: any) => {
                console.log('git onDidRunOperation');
                // if (!this._currentBranch) {
                //     this._currentBranch = this._repo?._repository?._HEAD?.name;
                //     console.log('We did\'t have a branch and git ran an operation. Now branch is', this._currentBranch);
                // }
                // if ((ev.operation === "Checkout") && !ev.error) { //ev.operation === "Branch" || 
                //     this._currentBranch = this._repo?._repository?._HEAD?.name;
                //     console.log('User changed branches. The new branch is', this._currentBranch);
                // }
            });
        }

        if (this._git.repositories.length) {
            console.log('The git api has already been initialized. Extracting repo and branch.');
            init();
        } else {
            console.log('Waiting for git api to be initialized.');
            this._git.onDidChangeState((e: any) => {
                console.log('Recieved git init event. Extracting repo and branch.');
                init();
             });
        }
    }

    private async _hasCredentials(): Promise<boolean> {
        const username = await this._context.secrets.get(`${this._url}_remote_username`);
        const password = await this._context.secrets.get(`${this._url}_remote_password`);
        return this._url !== undefined && username !== undefined && password !== undefined;
    }

    private async _doLogin() {
        const hasCredentials = await this._hasCredentials();
        if (!hasCredentials) {
            this._logChannel.appendLine('Please add remote');
            return;
        }
        console.log('Attempting login');
        this.login()
          .then(() => this._devKitEvents.emit(DevKitEventType.Login))
          .catch(error => this._logChannel.appendLine(`Error on login ${error.message ? error.message : ''}`));
    }

    private _initSSELogs() {
        this._devKitEvents.on(DevKitEventType.Login, () => this._openSSELogs());
        this._devKitEvents.on(DevKitEventType.AddCredentials, () => {
            this._closeSSELogs();
            this._doLogin();
            this._openSSELogs();
        });
        this._devKitEvents.on(DevKitEventType.RemoveCredentials, () => this._closeSSELogs());

        this._sseLogsEvents = this._sseLogsService.getSEELogsEvents();
        this._sseLogsEvents.on(SSELogsConnectionEventType.Unauthorized, () => {
            this._sseCloseTimeout = setTimeout(() => this._doLogin(), 5000);
        });
        this._sseLogsEvents.on(SSELogsConnectionEventType.Error, () => {
            this._sseCloseTimeout = setTimeout(() => this._openSSELogs(), 5000);
        });

        this._doLogin();
    }

    private _openSSELogs() {
        if (!this._url) {
            return;
        }

        if (this._sseCloseTimeout) {
            clearTimeout(this._sseCloseTimeout);
        }

        const authorizationCookie = (this._axios.defaults.headers as any).Cookie;
        if (authorizationCookie === '') {
            return;
        }
        this._sseLogsConnection = this._sseLogsService.openSSELogs(this._url, authorizationCookie);
    }

    private _closeSSELogs() {
        if (!this._sseLogsConnection) {
            return;
        }

        this._sseLogsService.closeSSELogs();
        this._sseLogsConnection.close();
        delete this._sseLogsConnection;
    }
}
