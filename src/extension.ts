// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { KodarisDevKit } from './devkit';

export function activate(context: vscode.ExtensionContext) {
	
	console.log('Congratulations, your extension "kodaris-development-kit" is now active!');

	const kod = new KodarisDevKit(context);

	context.subscriptions.push(vscode.commands.registerCommand('kod.addRemote', () => kod.addRemote()));

	context.subscriptions.push(vscode.commands.registerCommand('kod.removeRemote', () => kod.removeRemote()));

	context.subscriptions.push(vscode.commands.registerCommand('kod.push', () => {
		if (!kod.isValid()) {
			console.error('not allowing push because access denied');
			return;
		}
		kod.push();
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kod.SSELogs', () => {
		if (!kod.isValid()) {
			console.error('not allowing start SSE logs because access denied');
			return;
		}
		kod.startSSELogging();
	}));

	vscode.workspace.onDidRenameFiles(async (e) => {
		if (!kod.isValid()) {
			console.error('not allowing rename because access denied');
			return;
		}
		kod.renameFiles(e.files as any);
	})

	vscode.workspace.onDidCreateFiles(async (e) => {
		if (!kod.isValid()) {
			console.error('not allowing create because access denied');
			return;
		}
		kod.saveFiles(e.files as vscode.Uri[]);
	});


	vscode.workspace.onDidSaveTextDocument(async e => {
		if (!kod.isValid()) {
			console.error('not allowing save because access denied');
			return;
		}
		kod.saveFile(e.uri);
	});

	vscode.workspace.onDidDeleteFiles(async (e) => {
		if (!kod.isValid()) {
			console.error('not allowing delete because access denied');
			return;
		}
		kod.deleteFiles(e.files as vscode.Uri[]);
	});
}

// this method is called when your extension is deactivated
export function deactivate() {}
