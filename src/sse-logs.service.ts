import * as vscode from 'vscode';
import { EventEmitter } from 'events';
const EventSource = require('eventsource');

const ALLOWED_ERRORS_AMOUNT = 5;

export type EventSourceType = typeof EventSource;

export enum SSELogsConnectionEventType {
  Unauthorized = 'unathorized',
  Error = 'error',
}

export class SSELogsService {
  private _events = new EventEmitter();

  private _logsChannel: vscode.OutputChannel;

  private _errorsAmout = 0;

  constructor() {
    this._logsChannel = vscode.window.createOutputChannel('Kodaris Devkit Logs');
  }

  getSEELogsEvents(): EventEmitter {
    return this._events;
  }

  openSSELogs(url: string, authorizationCookie: string): EventSourceType {
    console.log('Run SSE Logs');

    const eventSource = new EventSource(`${url}/api/user/sse/logging/js/${encodeURIComponent('[GLOBAL]')}`, {
      headers: {
        'Connection': 'keep-alive',
        'Cookie': `${authorizationCookie}`
      },
    });

    eventSource.addEventListener('open', () => {
      this._errorsAmout = 0;
      this._logsChannel.appendLine(`SSE Logs connection opened on ${url}`);
    });

    eventSource.addEventListener('message', (event: { data: string; }) => {
      this._logsChannel.appendLine(Buffer.from(event.data, 'base64').toString());
    });

    eventSource.addEventListener('error', (error: any) => {
      if (error.status === 403) {
        console.log('[Connection error]: Unauthorized');
        this._events.emit(SSELogsConnectionEventType.Unauthorized);
      } else {
        console.log('[Connection error]: Something went wrong');
        if (this._errorsAmout < ALLOWED_ERRORS_AMOUNT) {
          this._events.emit(SSELogsConnectionEventType.Error);
        }
        this._errorsAmout++;
      }
      this._logsChannel.appendLine(JSON.stringify(error));
      this._logsChannel.appendLine('SSE Logs connection closed with error, reconnecting...');
      eventSource.close();
    });

    return eventSource;
  }

  closeSSELogs() {
    this._events.removeAllListeners();
    this._logsChannel.appendLine('SSE Logs closed');
  }
}
